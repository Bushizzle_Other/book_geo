$(function(){

    function txtSize(){
        var artH = $('.GEO__article-text_fullheight').outerHeight(),
            hDiff = window.innerHeight - $('.GEO__article-title').outerHeight() - $('.GEO__article-breadcrumbs').outerHeight() - artH;

        if(hDiff)$('.GEO__article-text_fullheight').height(artH + hDiff);
    }

    $(window).on('resize', function(){
        $('.GEO__article-title').width(window.innerWidth - $('.GEO__aside').width() - 25);

        txtSize();
    });

    setTimeout(function(){
        $('.GEO__article-title').width(window.innerWidth - $('.GEO__aside').width() - 25);
        txtSize();
    }, 300);

    $('.GEO__aside-item_slide').each(function(){

        var name = $(this).find('.GEO__aside-item-txt').text(),
            icon = $(this).find('.GEO__aside-item-img').attr('src'),
            content = $(this).find('.GEO__aside-item-content').html(),
            $sname = $('.GEO__slide-name');

        $(this).click(function(){

            $('body').addClass('fullscreen');

            $('.GEO__slide-name-img').attr('src', icon);
            $('.GEO__slide-name-txt').text(name);

            $('.GEO__slide-close').css({
                width: $sname.outerHeight() + 'px',
                height: $sname.outerHeight() + 'px'
            });

            $('.GEO__slide')
                .addClass('GEO__slide_active')
                .find('.GEO__article-text').html('').append(content);

        });

    });

    $('.GEO__slide-close').click(function(){
        $('body').removeClass('fullscreen');
        $('.GEO__slide').removeClass('GEO__slide_active');
    });
    
});
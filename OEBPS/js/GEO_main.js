$(function(){

    initTooltips($('.GEO__main-item-nav'));

    function txtSize(){


        var $t = $('.GEO__common-text-wrapper'),
            o = $t.offset().top,
            h = $t.outerHeight,
            d = window.innerHeight - h - o;

        if(d > 0) $t.height(h + d);
    }

    txtSize();

    $(window).on('resize', txtSize());

});
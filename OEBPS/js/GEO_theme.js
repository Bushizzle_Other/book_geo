$(function(){

    initTooltips($('.GEO__item-button'));

    $('.GEO__theme-content').masonry({
        // options
        fitWidth: true,
        itemSelector: '.GEO__theme-item',
        stamp: '.GEO__common-subtitle',
        gutter: 20
    });

    $(window).resize(function () {

        setTimeout(function(){

            $('.GEO__theme-content').masonry();
            $('.GEO__theme-upper-content, .GEO__theme-icontitle').width($('.GEO__theme-content').width() - 26); //margin*2


        }, 500);
    });

    setTimeout(function(){
        $(window).trigger('resize');
    }, 300);
    
});
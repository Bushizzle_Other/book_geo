$(function(){

    initTooltips($('.GEO__item-button'));

    $('.GEO__section-content').masonry({
        // options
        fitWidth: true,
        itemSelector: '.GEO__section-item',
        stamp: '.GEO__breadcrumbs',
        gutter: 20
    });

    $(window).resize(function () {

        setTimeout(function(){

            $('.GEO__section-content').masonry();

        }, 500);
    });

    $(window).trigger('resize');
    
});
if(!isTask && $.fn.tooltipster) window.initTooltips = function($tooltips){

    $tooltips.each(function(){

        $(this).tooltipster({
            content: $(this).find('.GEO__tooltip'),
            trigger: 'click',
            onlyOne: true,
            speed: 150,
            animation: 'swing',
            autoClose: false,
            minWidth: function(){
                return (window.innerWidth < 768) ? 320 : 494;
            },
            functionBefore: function(origin, continueTooltip){
                if($('.tooltipster-base').length)$('.GEO__item-button').tooltipster('hide');
                continueTooltip();
            },
            functionReady: function(origin, tooltip) {

                $(tooltip).find('.GEO__tooltip').append(
                    $('<div class="GEO__tooltip-close"></div>').on('click touchstart', function(e){
                        e.stopPropagation();
                        $(origin).tooltipster('hide');
                    })
                );
            }
        }).find('.GEO__tooltip').remove();
    });

};
var crossPlayer = {

    audio: new Audio(),
    prev: getSave('crossPlayer'),

    $el: null,
    $player: null,

    $play: null,
    $pause: null,
    $mute: null,
    $hide: null,
    $close: null,
    $name: null,
    $bar: null,
    $barFill: null,

    muted: false,
    minimized: false,
    barInterval: null,

    init: function(){

        this.audio.oncanplay = crossPlayer.play;

        var self = this;

        self.$player = $(
            '<div class="CP__player">' +
                '<div class="CP__player-wrap">' +
                    '<div class="CP__progressbar"><div class="CP__progressbar-fill"></div></div>' +
                    '<div class="CP__play"></div>' +
                    '<div class="CP__pause"></div>' +
                    '<div class="CP__name"></div>' +
                    '<div class="CP__mute"></div>' +
                    '<div class="CP__hide"></div>' +
                    '<div class="CP__close"></div>' +
                '</div>' +
            '</div>'
        ).appendTo($('body'));

        self.$play = $('.CP__play').hide();
        self.$pause = $('.CP__pause');
        self.$mute = $('.CP__mute');
        self.$hide = $('.CP__hide');
        self.$close = $('.CP__close');
        self.$name = $('.CP__name');
        self.$bar =  $('.CP__progressbar');
        self.$barFill = $('.CP__progressbar-fill');

        self.$play.on('click', self.play);
        self.$pause.on('click', self.pause);

        self.$mute.click(function() {
            self.audio.volume = self.muted;
            self.muted = !self.muted;

            self.$mute.toggleClass('CP__mute_muted');
        });

        self.$hide.on('click', function(){

            self.toggle( self.minimized ? 0 : -67 );

            self.minimized = !self.minimized;
        });
        self.$close.on('click', self.close);

        self.$bar.click(function(){



        });

        if(isTask && self.prev){
            self.audio.src = self.prev.src;
            self.audio.time = self.prev.time;
            self.toggle(0);
            self.play();
        }
    },
    updateBar: function(){
        this.$barFill.css('width', this.$bar.width() / (this.audio.duration / this.audio.currentTime));
    },
    toggle: function(pos){

        this.$player.css('bottom', pos);

    },
    construct: function(src, time){

        this.audio.currentTime = time || 0;
        this.audio.src = src;

        this.$name.html(this.$el.attr('data-cplayer-name') || src.split('/')[src.split('/').length - 1]);

        this.toggle(0);

    },
    play: function(){
        crossPlayer.audio.play();
        crossPlayer.$play.hide();
        crossPlayer.$pause.show();

        this.barInterval = setInterval(function(){crossPlayer.updateBar()}, 50);
    },
    pause: function(){
        crossPlayer.audio.pause();

        crossPlayer.$play.show();
        crossPlayer.$pause.hide();

        clearInterval(this.barInterval);

        setSave('crossPlayer', {
            src: this.src,
            time: this.time
        });
    },
    close: function(){
        crossPlayer.audio.pause();

        crossPlayer.toggle(-77);
        crossPlayer.minimized = false;
        crossPlayer.muted = false;
        crossPlayer.audio.volume = 1;

        setSave('crossPlayer', null);
    }
};

$(function(){

    crossPlayer.init();

    $('*[data-cplayer-src]').click(function(){

        crossPlayer.$el = $(this);
        crossPlayer.construct($(this).attr('data-cplayer-src'), $(this).attr('data-cplayer-time'));

    });

});
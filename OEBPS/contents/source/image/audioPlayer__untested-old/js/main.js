$(function(){

    $check.add($restart).add($retry).add($text).add($header).remove();

    var src;

    if(config.content.indexOf('.mp3') != -1){

        src = config.contentFolder + config.content.split('/')[1];
        audioPreload(src);
        afterTaskInit();

    } else {

        loadXml(function(){
            $sandbox.append(xml);
            src = config.contentFolder + $sandbox.find('audio').text() + '.mp3';
            audioPreload(src);
        });

    }

    function audioPreload(src){
        var audio = document.createElement('audio');
        audio.src = src;
        audio.autoplay = true;
        audio.controls = true;
        $area.append(audio);
        $(window).trigger('resize');
    }

});
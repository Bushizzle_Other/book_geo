$(function(){

    $check.add($restart).add($retry).add($text).add($header).remove();

    var src;

    if(config.content.indexOf('.mp4') != -1){

        src = config.contentFolder + config.content.split('/')[1];
        videoPreload(src);
        afterTaskInit();

    } else {

        loadXml(function(){
            $sandbox.append(xml);
            src = config.contentFolder + $sandbox.find('video').text() + '.mp4';
            videoPreload(src);
        });

    }

    function videoPreload(src){
        var video = document.createElement('video');
        video.controls = true;
        $area.append(video);

        video.oncanplay = function(){

            $(video).css({
                display: 'block'
            });

            var ratio = $(video).width() / $(video).height();

            $(window).resize(function (){

                if(window.innerWidth > window.innerHeight*ratio)$(video).css({height: '95%', width: 'auto', margin: '0 auto'});
                else $(video).css({width: '90%', height: 'auto', margin: '0 5%'});

            });

            video.play();

            $(window).trigger('resize');
        };
        video.src = src;
    }

});
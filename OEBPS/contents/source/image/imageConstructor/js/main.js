$(function(){
    $check.add($restart).add($retry).hide();
    $loadingOverlay.show();
    $('#stageContainer').append($header, $text, $area);

    var $creator = $('<div class="IC__create">Создать картинку</div>'),
        $downloader = $('<a href="#" class="IC__download">Скачать</a>'),
        $bgChanger = $('<div class="IC__change-bg">Сменить фон</div>');

    var $bgPopupArea = $('<div class="IC__popup-area"></div>'),
        $bgPopup = $('<div class="IC__popup"></div>')
            .append($bgPopupArea)
            .append($('<div class="IC__popup-close"></div>')
                .click(function(){
                    $bgPopup.fadeOut();
                    $blurOverlay.fadeOut();
                }))
            .appendTo($('body'));

    var $buttons = $('.cp-button-container');

    $buttons.append($creator, $downloader, $bgChanger);

    var $dropArea = $('<div class="IC__drop" id="scene"></div>'),
        $dragsArea = $('<div class="IC__drags"></div>');

    var scene,
        backgroundRatio,
        fileName;

    var backgrounds = [],
        images = [];

    loadLibs([
        ['$.ui','core/scripts/libs/jquery-ui-1.9.2.custom.min.js'],
        ['html2canvas','core/scripts/libs/html2canvas.js'],
        ['dataURLtoBlob','core/scripts/libs/canvas-to-blob.min.js']
    ]);

    loadXml(function(){

        $sandbox.append(xml);

        $header.append($sandbox.find('title').html());
        $text.append($sandbox.find('question').html());

        $sandbox.find('.backgrounds').children().each(function(){
            var base64string = $(this).attr('src');
            backgrounds.push(base64string);

            $bgPopupArea.append(
                $('<div class="IC__popup-image">')
                    .append($('<img src="' + base64string +'" alt=""/>'))
                    .click(function(){
                        initScene($(this).find('img').attr('src'), true);
                        $blurOverlay.fadeOut();
                        $bgPopup.fadeOut();
                    })
            );
        });

        $('.IC__popup-image img').on('dragstart', function(e) { e.preventDefault(); });

        $sandbox.find('.images').children().each(function(){
            images.push($(this).attr('src'));
        });

        fileName = $sandbox.find('filename').text();

        $downloader.attr('download', fileName);

        $sandbox.find('style').appendTo('body');

        loadLibs([['$.ui.rotatable','core/scripts/libs/jquery-ui-rotatable.js']], false, initTask);

    });

    function initTask(){

        initDrop();
        initDrags();

        $area.append($dragsArea, $dropArea);

        $loadingOverlay.fadeOut();

        $creator.click(function(){
            if(!isIE)$creator.hide();
            $loadingOverlay.fadeIn();

            $area.addClass('IC__screenshot');
            takeScreenshot();
        });

        $downloader.click(function(){
            $creator.show();
            $downloader.hide();
        });

        $bgChanger.click(function(){
            $blurOverlay.fadeIn();
            $bgPopup.fadeIn();
        });

        $(window).trigger('resize');
    }

    function throttle(func, ms) {

        var isThrottled = false,
            savedArgs,
            savedThis;

        function wrapper() {

            if (isThrottled) { // (2)
                savedArgs = arguments;
                savedThis = this;
                return;
            }

            func.apply(this, arguments); // (1)

            isThrottled = true;

            setTimeout(function() {
                isThrottled = false; // (3)
                if (savedArgs) {
                    wrapper.apply(savedThis, savedArgs);
                    savedArgs = savedThis = null;
                }
            }, ms);
        }

        return wrapper;
    }

    var resizeScene = throttle(function(){
        var resWidth,
            resHeight,
            areaHeight = window.innerHeight - $buttons.outerHeight() - $header.outerHeight() - $text.outerHeight() - 110,
            areaRatio = $area.width() / areaHeight;

        if(backgroundRatio > areaRatio){

            resWidth = window.innerWidth;
            resHeight =  'auto'

        } else {

            resWidth = 'auto';
            resHeight =  areaHeight;

        }

        $(scene).css({width: resWidth, height: resHeight});

        $dropArea.css({width: $(scene).width(), height: $(scene).height()});


    }, 200);

    function initDrags(){

        var $dragsWrap = $('<div class="IC__drags-wrap"></div>'),
            $dragsMeter = $('<div class="IC__drags-wrapmeter"></div>');

        $dragsArea.append($dragsWrap.append($dragsMeter));

        images.forEach(function(item){


            var $item = $('<img class="IC__item" src="' + item + '">').draggable({
                cursorAt: {
                    left: 50,
                    top: 10
                },
                helper: function() {
                    return $(this).clone().addClass('IC__dragging').css({zIndex: 100, opacity:.8});
                }
            });

            $dragsMeter.append($item);

        });

        function animateScroll($el, way){
            var scroll = $el.scrollLeft() + way;
            $el.animate({scrollLeft:scroll}, '500');
        }

        var $dragsLeft = $('<div class="LC__drags-left"></div>').click(function(){
                animateScroll($dragsWrap, -100);
            }),
            $dragsRight = $('<div class="LC__drags-right"></div>').click(function(){
                animateScroll($dragsWrap, 100);
            });

        $dragsArea.append($dragsLeft, $dragsRight);

        if($dragsMeter.width() > $dragsWrap.width()) $dragsLeft.add($dragsRight).fadeIn();

        $(window).resize(function(){
            if($dragsMeter.width() > $dragsWrap.width()) $dragsLeft.add($dragsRight).fadeIn();
            else $dragsLeft.add($dragsRight).fadeOut();
        });

    }

    function initDrop(){
        scene = new Image();
        scene.className = 'IC__image';
        $(scene).on('dragstart', function(event) { event.preventDefault(); });

        $dropArea.append(scene);

        $dropArea.droppable({
            accept: ".IC__item",
            drop: function(event, ui) {

                if(!$(ui.draggable).hasClass('IC__in')){

                    var $drag = $(ui.draggable).clone().addClass('IC__in').removeClass('IC__dragging'),
                        $dragging = $('.IC__dragging'),
                        $resizer = $('<div class="IC__resizer">'),
                        $delDrag = $('<div class="IC__del"></div>'),
                        $rotator = $('<div class="IC__rotator"></div>'),
                        id = $drag.attr('id');

                    $drag
                        .attr('data-id', id)
                        .removeAttr('id');

                    $delDrag.click(function(){
                        $resizer.remove();
                    });

                    $resizer.append($rotator.append($drag), $delDrag);

                    $resizer.draggable({
                        containment: ".IC__drop",
                        scroll: false,
                        stop: function() {
                            $(this).css({
                                left: $(this).parseCss("left") / $dropArea.width() * 100 + "%",
                                top: $(this).parseCss("top") / $dropArea.height() * 100 + "%"
                            });
                        }
                    });

                    $resizer.resizable({
                        containment: ".IC__drop",
                        aspectRatio: true,
                        handles: 'ne, se, sw, nw',
                        stop: function() {
                            $(this).css({
                                width: $(this).width() / $dropArea.width() * 100 + "%",
                                height: $(this).height() / $dropArea.height() * 100 + "%"
                            });
                        }
                    });

                    $rotator.rotatable();

                    $(this).append($resizer);

                    $resizer.css({
                        position: 'absolute',
                        left: ($dragging.offset().left - $dropArea.offset().left) / $dropArea.width() * 100 + "%",
                        top: ($dragging.offset().top - $header.outerHeight() - $text.outerHeight() - $dragsArea.height()) / $dropArea.height() * 100 + "%",
                    });

                    $resizer.css({
                        width: $resizer.width() / $dropArea.width() * 100 + '%',
                        height: $resizer.height() / $dropArea.height() * 100 + '%'
                    });

                }
            }
        });

        taskResizeHolder = resizeScene;

        initScene(backgrounds[0]);
    }

    function initScene(src, rebuild){

        scene.src = src;

        scene.onload = function(){
            scene.onload = null;
            backgroundRatio = $(scene).width() / $(scene).height();

            resizeScene();
        };
    }

    takeScreenshot = function(){
        html2canvas(document.getElementById('scene'),{
            onrendered: function(canvas) {

                canvas.toBlob(
                    function (blob) {

                        if(isIE){
                            window.navigator.msSaveBlob(blob, fileName + '.png');
                        } else {

                            $downloader.attr('href', window.URL.createObjectURL(blob)).css('display', 'inline-block');

                        }

                        $area.removeClass('IC__screenshot');
                        $loadingOverlay.fadeOut();

                    },
                    "image/png"
                );
            }
        });
    };

});
$(function(){

    var $connectorArea = $('<div/>').addClass('CN__area'),
        $canvas = $('<canvas/>').addClass('CN__canvas'),
        topOffset,
        areaWidth,
        areaHeight,
        correctPairs = [],
        readyPairs = [];

    var vars = [],
        varsMap = {},
        varsMapRevsrted = {},
        correctIdPairs = [];

    var resultMode;
    var blocked = false;
    var arrowSize, lineWidth;

    var imagesLoaded = false;

    loadLibs([['device','core/scripts/libs/device.min.js'],['$.fn.draw','core/scripts/libs/jcanvas.min.js']]);

    loadXml(function(){

        arrowSize = (isMobile)? 35 : 15;
        lineWidth = (isMobile)? 14 : 8;

        $sandbox.append(xml);

        $header.append($sandbox.find('title').html());
        $text.append($sandbox.find('question').html());

        resultMode = $sandbox.find('component').attr('mode') || 'errorsonly';

        initTask();
        preloadAllImages();

        $sandbox.find('style').appendTo('body');

        $check.click(checkTask);
        $restart.click(restartTask);
        $retry.click(restartTask);

        taskResizeHolder = function(){
            setImagesVarsHeight();
            resizeConnector();
            $loadingOverlay.show();

            setTimeout(function(){
                if(blocked)restartTask();
                resizeConnector();
                $loadingOverlay.hide();
            }, 200);
        };
    });

    function initTask(){

        $sandbox.find('bond').each(function(){
            correctPairs.push([$(this).attr('id1'), $(this).attr('id2')]);
        });

        $sandbox.find('column').each(function(){

            var col = {
                title: $(this).find('title').html(),
                elements: []
            };

            $(this).find('statement').each(function(){

                var elText = ($(this).attr('picture')) ? '<img class="CN__col-img" src="' + config.contentFolder + $(this).attr('picture') + '" data-txt="' + $(this).html() + '"><br><span class="CN__col-img">' + $(this).html() + '</span>' : $(this).html();

                col.elements.push({
                    id: $(this).attr('id'),
                    text: elText,
                    isImage: (elText.indexOf('<img') != -1) ? 'CN__col-var_img' : 'CN__col-var_txt'
                });

            });

            vars.push(col);

        });

        topOffset = $header.outerHeight() + $text.outerHeight();

        //

        vars.forEach(function(item, i){

            var $connectorColVars = $('<div/>').addClass('CN__col-vars'),
                $connectorCol = $('<div/>')
                    .addClass('CN__col')
                    .append(
                    $('<div/>').addClass('CN__col-title').append(item.title),
                    $('<div/>').addClass('CN__col-vars-wrapper').append($connectorColVars)
                );

            item.elements = shuffle(item.elements);

            var isFirstCol = i == 0,
                isLastCol = i == vars.length - 1,
                isDouble = isFirstCol == isLastCol;

            item.elements.forEach(function(element){

                var $var = $('<div/>')
                    .addClass('CN__col-var')
                    .addClass(element.isImage)
                    .append(element.text);

                if(!isFirstCol){
                    var fromId = randomId();
                    $var.append(
                        $('<div/>')
                            .addClass('CN__from')
                            .attr('id', fromId)
                            .attr('data-col', i)
                    );
                    varsMap[fromId] = element.id;
                    if(!isDouble)varsMapRevsrted[element.id] = [fromId];
                }

                if(!isLastCol){
                    var toId = randomId();
                    $var.append(
                        $('<div/>')
                            .addClass('CN__to')
                            .attr('id', toId)
                            .attr('data-col', i)
                    );
                    varsMap[toId] = element.id;
                    if(!isDouble)varsMapRevsrted[element.id] = [toId];

                }

                if(isDouble)varsMapRevsrted[element.id] = [fromId, toId];

                $connectorColVars.append($var);
            });

            $connectorArea.append($connectorCol);

        });

        $area.append($connectorArea);

        correctPairs.forEach(function(pair){
            var id1 = varsMapRevsrted[pair[0]],
                id2 = varsMapRevsrted[pair[1]];

            if(id1.length == 2 && id2.length == 2) console.warn('Капитан, у нас 4 колонкиб мы тонем! Нужна срочная доработка алгоритма составления правильных пар id элементов соединения');
            else {
                if(id1.byteLength == id2.length) correctIdPairs.push([id1[0], id2[0]]);
                else {

                    var singleId, doubleId;

                    if(id1.length == 2) {
                        singleId = id2[0];
                        doubleId = id1;
                    }
                    else {
                        singleId = id1[0];
                        doubleId = id2;
                    }

                    var $el = $('#' + singleId),
                        searchingClass = ($el.hasClass('CN__from')) ? 'CN__to' : 'CN__from',
                        anotherId = doubleId[0];

                    if(!$('#' + anotherId).hasClass(searchingClass)) anotherId = doubleId[1];

                    correctIdPairs.push([singleId, anotherId]);

                }
            }

        });

        $('.CN__col')
            .css('width', 100/vars.length + '%')
            .eq(-1).css('padding-right', 0);

    }

    function preloadAllImages(){
        var $images = $('#stageContainer').find('img'),
            counter = 0;

        function finishLoading(){

            setImagesVarsHeight();
            setCanvasLayer();
            resizeConnector();

            $loadingOverlay.hide();

            imagesLoaded = true;
        }

        if($images.length){

            $images.each(function(){
                $(this).load(function(){

                    counter++;
                    if(counter == $images.length)finishLoading();

                });
            });

            setTimeout(function(){
                if(!imagesLoaded){

                    var isSlow = confirm("Внимание, загружены не все картинки. Возможно, пути к ним указаны неверно. Или же у вас медленное интернет-соединение. В любом случае, без загруженных картинок задание не может быть построено корректно. Ждать загрузки дальше? ");
                    if(!isSlow) finishLoading();

                }
            }, 3000);

        } else finishLoading();

    }

    function setImagesVarsHeight(){

        $('.CN__col').each(function(){

            var $thisImages = $(this).find('.CN__col-var_img');

            if($thisImages.length){

                var areaHeight = window.innerHeight - topOffset - $(this).find('.CN__col-title').outerHeight() - $('.cp-button-container').outerHeight();

                $(this).find('.CN__col-var_txt').each(function(){
                    areaHeight -= $(this).outerHeight();
                });

                if($thisImages.length > 1)$(this).find('.CN__col-vars-wrapper').addClass('no-va');

                var maxImageHeight = areaHeight/$thisImages.length;

                $thisImages
                    .addClass('no-outer')
                    .find('img').each(function(){

                        var naturalHeight = $(this).height();

                        if(maxImageHeight < naturalHeight) $(this).css('height', maxImageHeight);

                    });

            }




        });

    }

    function setCanvasLayer(){

        var isDrawing = false,
            $activeEl = false,
            startX, startY,
            elClasses,
            elDrawingClass,
            elEndClass;

        areaWidth = $connectorArea.width();
        areaHeight = $connectorArea.height();

        $canvas.attr("width",  areaWidth);
        $canvas.attr("height",  areaHeight);

        $connectorArea
            .append($canvas)
            .bind('mousedown touchstart', function(e){

                $activeEl = $(e.target);
                elClasses = $activeEl[0].classList;

                if(elClasses.contains('CN__col-img')){
                    showFullscreenImage($activeEl.attr('src'), $activeEl.attr('data-txt'));
                    e.stopPropagation();
                    e.preventDefault();

                    $activeEl = null;
                    elClasses = null;

                } else if(!blocked){

                    resizeConnector();

                    if(elClasses.contains('CN__to')) {
                        elDrawingClass = 'CN__to';
                        elEndClass = 'CN__from';
                    }
                    if(elClasses.contains('CN__from')) {
                        elDrawingClass = 'CN__from';
                        elEndClass = 'CN__to';
                    }
                    if(elClasses.contains('CN__col-img')){
                        showFullscreenImage($activeEl.attr('src'), $activeEl.attr('data-txt'));
                    }

                    if(elDrawingClass){
                        e.stopPropagation();
                        e.preventDefault();

                        //prevent border-radius target bug
                        if($activeEl.hasClass('CN__from') || $activeEl.hasClass('CN__to')){

                            isDrawing = true;
                            $activeEl.addClass('hover');

                            startX = $activeEl.offset().left + $activeEl.outerWidth()/2;
                            startY = $activeEl.offset().top + $activeEl.outerHeight()/2 - topOffset;

                        }
                    }

                }


            })
            .bind('mouseup touchend', function(e){

                if(!blocked && $activeEl){

                    $activeEl.removeClass('hover');

                    resizeConnector();

                    isDrawing = false;

                    var $endEl;

                    if(isMobile){
                        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
                        $endEl = $(document.elementFromPoint(touch.pageX, touch.pageY));
                    }
                    else $endEl = $(e.target);

                    if($endEl.length){
                        e.stopPropagation();
                        e.preventDefault();

                        var col1 = +$activeEl.attr('data-col'),
                            col2 = +$endEl.attr('data-col'),
                            isSiblings = Math.abs(col2 - col1) == 1;

                        if($endEl[0].classList.contains(elEndClass) && isSiblings){

                            readyPairs.push([$activeEl.attr('id'), $endEl.attr('id')]);

                            $activeEl.addClass('connected');
                            $endEl.addClass('connected');

                        }

                    }

                    updateCanvas();

                }

                elDrawingClass = null;
                elClasses = null;

            })
            .bind('mousemove touchmove', function(e){

                if(!blocked && isDrawing && $activeEl){
                    e.stopPropagation();
                    e.preventDefault();

                    var x2, y2;

                    if(isMobile){

                        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];

                        x2 = touch.pageX;
                        y2 = touch.pageY;

                    } else {
                        x2 = e.pageX;
                        y2 = e.pageY;
                    }

                    $canvas.clearCanvas();

                    var scrollTop = $('#stageContainer').scrollTop();

                    updateCanvas();

                    $canvas
                        .drawLine({
                            strokeStyle: '#80D0DE',
                            strokeWidth: lineWidth,
                            rounded: true,
                            endArrow: true,
                            arrowRadius: arrowSize,
                            arrowAngle: 40,
                            x1: startX,
                            y1: startY + scrollTop,
                            x2: x2 + 2,
                            y2: y2  - topOffset + scrollTop
                        });


                }

            });
    }

    function updateCanvas(errors){

        var scrollTop = $('#stageContainer').scrollTop();

        $canvas.clearCanvas();

        if(errors){

            if(resultMode == 'showall'){
                correctIdPairs.forEach(function(item){

                    var $from = $('#' + item[0]),
                        $to = $('#' + item[1]);

                    $canvas
                        .drawLine({
                            strokeStyle: '#97c25a',
                            strokeWidth: lineWidth,
                            rounded: true,
                            x1:  $from.offset().left + $from.outerWidth()/2,
                            y1: $from.offset().top + $from.outerHeight()/2  - topOffset + scrollTop,
                            x2:  $to.offset().left + $to.outerWidth()/2,
                            y2: $to.offset().top + $to.outerHeight()/2  - topOffset + scrollTop
                        });

                    $from.removeClass('connected').addClass('correct_int');
                    $to.removeClass('connected').addClass('correct_int');

                });
            }

            readyPairs.forEach(function(item){

                var $from = $('#' + item[0]),
                    $to = $('#' + item[1]);

                $canvas
                    .drawLine({
                        strokeStyle: '#80D0DE',
                        strokeWidth: lineWidth,
                        rounded: true,
                        x1:  $from.offset().left + $from.outerWidth()/2,
                        y1: $from.offset().top + $from.outerHeight()/2  - topOffset + scrollTop,
                        x2:  $to.offset().left + $to.outerWidth()/2,
                        y2: $to.offset().top + $to.outerHeight()/2  - topOffset + scrollTop
                    });

                $from.removeClass('connected').addClass('connected_int');
                $to.removeClass('connected').addClass('connected_int');

            });

            errors.forEach(function(item){

                var $from = $('#' + item[0]),
                    $to = $('#' + item[1]);

                $canvas
                    .drawLine({
                        strokeStyle: 'red',
                        strokeWidth: lineWidth,
                        rounded: true,
                        x1:  $from.offset().left + $from.outerWidth()/2,
                        y1: $from.offset().top + $from.outerHeight()/2  - topOffset + scrollTop,
                        x2:  $to.offset().left + $to.outerWidth()/2,
                        y2: $to.offset().top + $to.outerHeight()/2  - topOffset + scrollTop
                    });

                $from.removeClass('connected').addClass('error_int');
                $to.removeClass('connected').addClass('error_int');

            });

        } else {

            readyPairs.forEach(function(item){

                var $from = $('#' + item[0]),
                    $to = $('#' + item[1]);

                $canvas
                    .drawLine({
                        strokeStyle: '#80D0DE',
                        strokeWidth: lineWidth,
                        rounded: true,
                        x1:  $from.offset().left + $from.outerWidth()/2,
                        y1: $from.offset().top + $from.outerHeight()/2  - topOffset + scrollTop,
                        x2:  $to.offset().left + $to.outerWidth()/2,
                        y2: $to.offset().top + $to.outerHeight()/2  - topOffset + scrollTop
                    });

                $from.addClass('connected');
                $to.addClass('connected');

            });
        }

    }

    function resizeConnector(){

        if(!blocked){

            var scrollTop = $('#stageContainer').scrollTop(); // TODO вспомнить нафига это нужно
            topOffset = $header.outerHeight() + $text.outerHeight();

            areaWidth = $connectorArea.width();
            areaHeight = $connectorArea.height();

            $canvas.attr("width",  areaWidth);
            $canvas.attr("height",  areaHeight);

            $('.CN__col-vars').each(function(){

                var $parent = $(this).parent(),
                    $children = $(this).find('.CN__col-var');

                var varsHeight = $(this).outerHeight(),
                    varsSpace = window.innerHeight - topOffset - $('.cp-button-container').outerHeight() - 30,
                    childrenNum = $children.length,
                    possibleMargin = (varsSpace - varsHeight) / ( childrenNum/2 );

                if(varsHeight < varsSpace){

                    $parent.height(varsSpace);


                } else {
                    $parent.css('height', 'auto');
                }
            });

            updateCanvas();

        }

    }

    function checkTask(){

        resizeConnector();

        blocked = true;

        var errors = false, mistakes = [], connected = [], missed = correctPairs.slice();

        readyPairs.forEach(function(pair){

            var id1 = varsMap[pair[0]],
                id2 = varsMap[pair[1]],
                gotPair = false;

            correctPairs.forEach(function(correctPair){

                if(correctPair.indexOf(id1) != -1 && correctPair.indexOf(id2)  != -1) {
                    gotPair = true;
                }
            });

            if(!gotPair) {

                errors = true;
                mistakes.push(pair);

            } else connected.push(id1, id2);

        });

        if(errors || !readyPairs.length || readyPairs.length < correctPairs.length) {

            missed.forEach(function(pair, i){
                connected.forEach(function(connectedOne){
                    if(pair.indexOf(connectedOne[0]) != -1 && pair.indexOf(connectedOne[1]) != -1) missed.splice(i, 1);
                });
            });

            resultShow(false);

            if(readyPairs.length) updateCanvas(mistakes);
            else {
                updateCanvas([]);
            }

        }
        else {
            resultShow(true);
        }

    }

    function restartTask(){

        $canvas.clearCanvas();
        readyPairs = [];

        $('.correct_int, .error_int, .connected_int, .hover, .connected').removeClass('correct_int error_int connected_int hover connected');

        $('.CN__col-vars').each(function(){
            $(this).children().shuffle();
        });

        blocked = false;
        resizeConnector();

    }

});
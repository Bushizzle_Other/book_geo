$(document).ready(function(){

	var $tasks = $(".write"),
		$bigtasks = $(".bg"),
		tasksNum = $tasks.length,
		$drop = $('.task-retry'),
		$textmeter = $('<div class="textmeter"></div>').appendTo('body');

	$check.addClass('check_blocked');


	$tasks.add($bigtasks).each(function(){
		var $self = $(this);

		if($self[0].nextSibling.nodeType == 3) $self.addClass('margin-fix');

		$self
			.on('focus', function(){
				$textmeter.css({
					fontSize: $self.css('fontSize'),
					fontFamily: $self.css('fontFamily')
				});
			})
			.on('keyup', function(){

				var val = $self.val();

				$textmeter.text(val);
				$self.width($textmeter.width() + 3);

				if(val.length)$self.removeClass('hold');

				if(!$('.hold').length)$check.removeClass('check_blocked');

			});
	});

	$drop.click(function(){
		$tasks.val('').removeAttr("style").addClass('hold').removeClass('input_error input_ready');
		$check.addClass('check_blocked');
		//$drop.removeClass('showme_cell');
	});


	$check.click(function(){

		if(!$check.hasClass('check_blocked')){

			$tasks.each(function(){
				var val = $(this).val();
				if(val != '' && $(this).attr('data-res').split('$').indexOf(val) != -1) $(this).addClass('input_ready').removeClass('input_error');
				else $(this).addClass('input_error').removeClass('input_ready');
			});

			//$drop.removeClass('showme_cell');

			if($('.input_ready').length == tasksNum){
				resultShow(true);
			}
			else {
				resultShow(false);
			}

		}

	});

	$restart.click(function(){

		$tasks.val('').css("width", "").addClass('hold').removeClass('input_error').removeClass('input_ready');
		$tasks.parents(".interakt_zadanie").find('.drop').removeClass('showme_cell');
		$check.addClass('check_blocked');
	});

});
$(function(){

    var $textArea = $('<div/>').addClass('WP__text'),
        $wordsArea = $('<div/>').addClass('WP__words'),
        vars = [];

    var pusherClone = false;

    loadLibs([['$.ui','core/scripts/libs/jquery-ui-1.9.2.custom.min.js']]);

    loadXml(function(){

        $sandbox.append(xml);
        if($sandbox.find('component').attr('mode') == 'copy') pusherClone = true;

        $header.append($sandbox.find('title').html());
        $text.append($sandbox.find('question').html());

        initTask();

        $sandbox.find('style').appendTo('body');

    });

    function initTask(){

        $check.click(checkTask);

        $restart.click(restartTask);
        $retry.click(restartTask);


        //

        var answerArray = [];

        var text = '<span class="WP__text-node">' + $sandbox.find('text').html() + '</span>';

        text = text.replace(/#[^#]+#/gi, function(word){

            var answers = word.substr(1, word.length - 2).split('~'),
                answersAttr = [],
                isWord = (text.charAt(text.indexOf(word) - 1) == ' ' && text.charAt(text.indexOf(word) + word.length) == ' ');

            answers.forEach(function(item){

                if(item != ''){

                    if(answerArray.indexOf(item) == -1) answerArray.push(item);

                    answersAttr.push(item);
                }

            });


            //answerArray.push(answer);
            return '</span><span class="WP__drop WP__drop_waiting ' + ((isWord)?'':'WP__drop_letter') + '" data-val="' + answersAttr.join('#') + '">...</span><span class="WP__text-node">'
        });

        $textArea.append(text);

        var wrongAnswers = $sandbox.find('wrongWords').text().split('#');

        answerArray.forEach(function(item){
            vars.push($('<div/>').addClass('WP__drag').append(item).attr('data-val', item));
        });

        wrongAnswers.forEach(function(item){
            if(item.length) vars.push($('<div/>').addClass('WP__drag WP__fake').append(item).attr('data-val', item));
        });

        vars = shuffle(vars);

        vars.forEach(function(item){
            $wordsArea.append(item.attr('id', 'drag'+Math.round(Math.random()*100000)))
        });

        $area.append($textArea, $wordsArea);

        $('.WP__drag')
            .draggable({
                helper: function() {
                    return $(this).clone();
                },
                start: function(){

                },
                stop: function( event ) {

                }
            });

        $('.WP__drop').droppable({
            accept: ".WP__drag",
            drop: function(event, ui) {

                var $prevItem =  $(this).find('.WP__drag');

                if($prevItem.length){
                    $('#' + $prevItem.attr('data-id')).removeClass('disabled').draggable("enable");
                }

                var $drag = $(ui.draggable).clone(),
                    id = $drag.attr('id');

                $drag
                    .attr('data-id', id)
                    .removeAttr('id');

                if(!pusherClone){
                    $('#' + id)
                        .addClass("disabled")
                        .draggable("disable");
                }

                $drag
                    .click(function(){
                        $('#' + id)
                            .removeClass("disabled")
                            .draggable("enable");
                        $(this).parent()
                            .html('...')
                            .addClass('WP__drop_waiting')
                    });

                $(this)
                    .html('')
                    .css('width', 'auto')
                    .append($drag)
                    .removeClass('hover')
                    .removeClass('WP__drop_waiting');
            },
            over: function(){
                $(this).addClass('hover');
            },
            out: function() {
                $(this).removeClass('hover');
            }
        });
    }

    function checkTask(){

        var errors = false;

        $('.WP__text').find('.WP__drop').each(function(){

            var correct = $(this).attr('data-val').split('#'),
                current = $(this).find('.WP__drag').attr('data-val'),
                gotResult = false;

            if(current){

                correct.forEach(function(item, i, arr){

                    if(item == current) gotResult = true;
                    else if(i == arr.length - 1 && !gotResult) errors = true;

                });

            } else errors = true;

            if(gotResult) {
                $(this).addClass('correct_int');
            } else {
                $(this).addClass('error');
            }

        });

        if(errors) resultShow(false);
        else resultShow(true);

    }

    function restartTask(){

        $('.WP__drop').removeClass('error correct_int').addClass('WP__drop_waiting').html('...').droppable('enable');
        $('.WP__drag').removeClass('disabled').draggable('enable');

    }

});